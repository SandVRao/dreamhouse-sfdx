public class MaintenanceRequestHelper {
    /*
    Use the included package content to automatically create a Routine Maintenance request every time 
    a maintenance request of type Repair or Routine Maintenance is updated to Closed. 
    Follow the specifications and naming conventions outlined in the business requirements.
    */
    
    public static List<String> typeList = new List<String>();
    //public static List<Case> casesToBeUpdated = new List<Case>();
    //public static List<Case> newMaintenanceRequests = new List<Case>();
    
    public static void updateWorkOrders(Set<Id> newCases, Map<Id,Case> oldCases){
        List<Case> casesToBeUpdated = new List<Case>();
        List<Case> newMaintenanceRequests = new List<Case>();
        typeList.add('Repair');
        typeList.add('Routine Maintenance');
        
        // update workorders
        List<Case> updatedCases = [Select Id,status, type, vehicle__c, product__c, subject,Date_Reported__c, Date_Due__c, AccountId, origin, Equipment__c  from Case where Id IN: newCases AND type IN: typeList];
        System.debug('**updatedCases ' +updatedCases );
        for(Case c: updatedCases){
            if(oldCases.get(c.Id).Status <> 'Closed' && c.Status == 'Closed'){
                casesToBeUpdated.add(c);
            }
        }
        System.debug('***casesToBeUpdated'+casesToBeUpdated);
        for(Case caseToUpdate: casesToBeUpdated){
            Case newCase = new Case();
            newCase.Type = caseToUpdate.Type;
            newCase.Status = 'New';
            newCase.Vehicle__c = caseToUpdate.Vehicle__c;
            newCase.Product__c = caseToUpdate.Product__c;
            newCase.Subject =  String.isBlank(caseToUpdate.Subject) ?'Routine maintenance request':caseToUpdate.Subject;
            newCase.Date_Reported__c = Date.today();
            newCase.Date_Due__c = Date.today().addDays(2);
            
            newCase.AccountId = caseToUpdate.AccountId;
            newCase.Origin = caseToUpdate.Origin;
            newCase.Equipment__c = caseToUpdate.Equipment__c;
            
            newMaintenanceRequests.add(newCase);  
        }
            System.debug('***newMaintenanceRequests'+newMaintenanceRequests);
            if(!newMaintenanceRequests.isEmpty()){
                insert newMaintenanceRequests;
            }
        
    }        
    
}